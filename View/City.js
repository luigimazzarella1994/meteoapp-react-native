import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Axios from 'axios';
import HoursWeather from '../components/HoursWheater'


const APIKEY = 'ffa86372f2df0072c7af1aaf54ce5fcc';

const City = ({ route }) => {


    const title = route.params.title;
    const data = route.params.data;
    const temp = route.params.currentTemp;
    const coord = route.params.coord;

    const [hours, setHours] = useState([]);

    /*

    useEffect(() => {
        // getWeather();
    })
    const getWeather = () => {
        Axios.get(`https:\\api.openweathermap.org/data/2.5/onecall?lat=${coord.lat}&lon=${coord.lng}&APPID=${APIKEY}&lang=it&units=metric`)
            .then(data => {
                console.log(data)
                setHours(data.data.hourly.slice(0, 25))
            }).catch(error => {
                console.log(error)
            })
    } */

    /*
    const hourly = hours.map((item, index) =>
        <HoursWeather key={index} />
    ) */

    return (


        <LinearGradient
            colors={['#ffffff', '#33ccff']}
            style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                height: '100%',
            }
            }
        >
            <View style={styles.container} >
                <View style={styles.CurrentDay}>
                    <Text style={styles.degradStyle}>{temp}°</Text>
                    <Image
                        style={{ height: 70, width: 70 }}
                        source={{ uri: `https://openweathermap.org/img/wn/${data.icon}@2x.png` }}

                    />
                    <Text style={styles.weatherCondition}>{data.description}</Text>

                </View>
                <View style={styles.hoursContainer}>
                    <ScrollView
                        horizontal={true}
                        contentContainerStyle={styles.scrollView}
                    >
                        <HoursWeather></HoursWeather>
                        <HoursWeather></HoursWeather>
                        <HoursWeather></HoursWeather>
                        <HoursWeather></HoursWeather>
                        <HoursWeather></HoursWeather>
                        <HoursWeather></HoursWeather>
                        <HoursWeather></HoursWeather>
                        <HoursWeather></HoursWeather>

                    </ScrollView>
                </View>
                <View style={styles.dayContainer}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1 }}>
                        <Text style={{ padding: 10, fontSize: 20 }}>Giovedì</Text>
                        <Text style={{ padding: 10, fontSize: 20 }}>Icon</Text>
                        <Text style={{ padding: 10, fontSize: 20 }}>21</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1 }}>
                        <Text style={{ padding: 10, fontSize: 20 }}>Venerdì</Text>
                        <Text style={{ padding: 10, fontSize: 20 }}>Icon</Text>
                        <Text style={{ padding: 10, fontSize: 20 }}>21</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1 }}>
                        <Text style={{ padding: 10, fontSize: 20 }}>Sabato</Text>
                        <Text style={{ padding: 10, fontSize: 20 }}>Icon</Text>
                        <Text style={{ padding: 10, fontSize: 20 }}>22</Text>
                    </View>

                </View>
            </View>
        </LinearGradient>


    )

}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    CurrentDay: {
        flex: 2,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    hoursContainer: {
        flex: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: 'grey',
        width: '100%',

    },
    scrollView: {
        width: '100%',
        alignItems: 'center'
    },
    dayContainer: {
        flex: 2,
        width: '100%',
    },
    weatherCondition: {
        fontSize: 18
    },
    degradStyle: {
        fontSize: 75
    },

})

export default City;