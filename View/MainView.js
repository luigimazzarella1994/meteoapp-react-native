import React from 'react';
import { View, Text, StyleSheet, ScrollView, Button, Alert } from 'react-native';
import CityCard from '../components/CityCard';
import AddCityModal from '../components/AddCityModal';
import { LinearGradient } from 'expo-linear-gradient';


class MainView extends React.Component {


    state = {
        modal: false,
        cities: []
    }

    openModal = () => {
        this.setState({ modal: true })
    }

    closeModal = () => {
        this.setState({ modal: false })
    }

    addCity = (city) => {

        if (city.trim(city !== '')) {

            this.state.cities.push(city)
            this.closeModal()
        }
        else {
            alert("ADD SOMETHING")
        }
    }

    removeCity = (city) => {
        Alert.alert(
            "Remove City",
            "Do you want to remove this city?",
            [
                {
                    text: "Yes",
                    onPress: () => this.setState({ cities: this.state.cities.filter(cityName => cityName !== city) }),
                    style: "cancel"
                },
                { text: "No" }
            ],
            { cancelable: false }
        );


    }



    render() {

        const cities = this.state.cities.map((city, index) => (<CityCard removeCity={this.removeCity} navigation={this.props.navigation} key={index} title={city} />))

        return (
            <LinearGradient
                colors={['#ffffff', '#33ccff']}
                style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    height: '100%',
                }}
            >
                <View style={{ marginRight: 10, alignItems: 'flex-end', justifyContent: 'flex-end', marginTop: 20, marginBottom: 30 }}>
                    <Button
                        title="Add new city"
                        onPress={this.openModal}
                    />
                </View>
                <ScrollView contentContainerStyle={styles.container} >
                    <AddCityModal visible={this.state.modal} closeModal={this.closeModal} addCity={this.addCity} />
                    {cities}
                </ScrollView>

            </LinearGradient>
        )

    }


}


const styles = StyleSheet.create({

    container: {

        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },

})

export default MainView;