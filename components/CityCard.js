import React from 'react';
import { View, Text, StyleSheet, Modal, ImageBackground } from 'react-native';
import { TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Axios from 'axios';


const APIKEY = 'ffa86372f2df0072c7af1aaf54ce5fcc';

class CityCard extends React.Component {

    state = {
        deg: '',
        dataWeather: [],
        coord: {}
    }


    componentDidMount() {

        Axios.get(`https:\\api.openweathermap.org/data/2.5/weather?q=${this.props.title}&APPID=${APIKEY}&lang=it&units=metric`).then(data => {
            this.setState({ deg: Math.round(data.data.main.temp), dataWeather: data.data.weather[0], coord: { lat: data.data.coord.lat, lng: data.data.coord.lon } })

        }).catch(error => {
            console.log(error)
        })
    }

    setImage = () => {
        if (this.state.dataWeather.main === 'Clear') {
            return require('../assets/soleggiato.jpg')
        }
        else if (this.state.dataWeather.main === 'Clouds') return require('../assets/clouds.png')

    }





    render() {
        const Image = this.setImage()
        return (
            <TouchableOpacity style={styles.container}
                onPress={() => this.props.navigation.navigate('City', { title: this.props.title, data: this.state.dataWeather, currentTemp: this.state.deg, coord: this.state.coord })}
                onLongPress={this.props.removeCity.bind(this, this.props.title)}
            >
                <ImageBackground
                    source={Image}
                    style={styles.image}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={[styles.text, { marginLeft: 30, fontSize: 20, fontWeight: 'bold' }]}>{this.props.title}</Text>
                        <Text style={[styles.text, { marginRight: 30 }]}>{this.state.deg}°</Text>
                    </View>
                </ImageBackground>
            </TouchableOpacity>


        )
    }

}

const styles = StyleSheet.create({

    container: {

        borderRadius: 1,
        height: 60,
        margin: 10,
        backgroundColor: 'white',
        justifyContent: 'center',
        shadowColor: 'black',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.7,
        elevation: 10,
        width: 350,

    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        borderRadius: 10,
        backgroundColor: 'white'
    },

    text: {
        fontSize: 20
    }

})


export default CityCard;