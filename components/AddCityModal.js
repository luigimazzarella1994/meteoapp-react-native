import React from 'react';
import { View, Text, StyleSheet, ScrollView, Modal, Button, TextInput } from 'react-native';
import Header from './Title';
import { Entypo } from '@expo/vector-icons'




class AddCityModal extends React.Component {

    state = {
        text: '',
    }

    textHandler = (value) => {
        this.setState({ text: value })

    }


    render() {
        return (
            <Modal visible={this.props.visible} animationType='slide' >
                <View style={styles.container}>
                    <View style={styles.buttonView}>

                        <Entypo
                            name="cross"
                            size={30}
                            color="red"
                            onPress={this.props.closeModal}
                            style={{ marginTop: 20 }}
                        />
                        <Text style={styles.titleStyle}>Aggiungi Citta</Text>
                        <View
                            style={{ marginTop: 10 }}>
                            <Button
                                title="Done"
                                onPress={this.props.addCity.bind(this, this.state.text)}
                            />
                        </View>

                    </View>

                    <View style={styles.containerTextView}>
                        <TextInput
                            style={styles.textInputContainer}
                            value={this.state.text}
                            onChangeText={this.textHandler}
                            placeholder="Search city"
                        >

                        </TextInput>
                    </View>
                </View>
            </Modal>
        )
    }




}

const styles = StyleSheet.create({

    container: {

        alignContent: 'center',
        justifyContent: 'center',
        padding: 4
    },

    buttonView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start'

    },

    containerTextView: {
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 100,
    },

    textInputContainer: {
        borderWidth: 1,
        borderRadius: 8,
        padding: 10,
        width: '80%'
    },
    titleStyle: {
        marginTop: 10,
        fontSize: 30,
        color: 'black',
        alignContent: 'flex-start',
        justifyContent: 'flex-start',
        fontWeight: 'bold'
    }

})

export default AddCityModal;