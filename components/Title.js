import React from 'react';
import { View, Text, StyleSheet } from 'react-native'


const Header = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.titleStyle}> {props.title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({

    titleStyle: {
        marginTop: 10,
        fontSize: 30,
        color: 'black',
        alignContent: 'flex-start',
        justifyContent: 'flex-start',
        fontWeight: 'bold'
    }
})

export default Header;