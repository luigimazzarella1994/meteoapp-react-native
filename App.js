import React from 'react';
import { StyleSheet, Text, View, Button, Modal } from 'react-native';
import MainView from './View/MainView';
import City from './View/City';
import { NavigationContainer, BaseRouter } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function App(props) {


  const Stack = createStackNavigator();
  return (

    <NavigationContainer >
      <Stack.Navigator>
        <Stack.Screen name="Meteo" component={MainView} />
        <Stack.Screen name="City" component={City} options={({ route }) => ({ title: route.params.title, data: route.params.data, currentTemp: route.params.currentTemp, coord: route.params.coord })} />
      </Stack.Navigator>

    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});

export default App;
